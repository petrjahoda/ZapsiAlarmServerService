package com.company;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import com.ztomic.wkhtmltopdf.WkHtmlToPdf;
import com.ztomic.wkhtmltopdf.argument.Argument;
import com.ztomic.wkhtmltopdf.argument.Option;
import com.ztomic.wkhtmltopdf.source.Source;
import org.apache.commons.io.FileUtils;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import static java.nio.charset.StandardCharsets.UTF_8;

public class Main {
    private static int port;
    private static String databaseAddress;
    private static String databaseName;
    private static String userName;
    private static String userPassword;
    private static String customer;
    private static String emailsFrom;
    private static String emailsTo;
    private static String smtpHost;
    private static String smtpPassword;
    private static Connection mySQLConnection;
    private static String SSL;
    private static int numberOfAlarms;
    public static final String BUILD_NUMBER = "30";
    public static final String BUILD_DATE = "20171213-114543";

    public static void main(String[] args) throws IOException, InterruptedException {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd-HHmmss");
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd. MM. yyyy,  HH:mm:ss");
        Date date = null;
        try {
            date = format.parse(BUILD_DATE);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String outDateVersion = outputFormat.format(date);
        createSettingsFileIfNotExist();
        loadSettingsFromFile();
        sendEmail("Alarm server started", emailsTo, "", "", 0);
        createLogFileIfNotExist();
        openDatabaseConnection();
        writeInformationToLogFile("Running version 1.5." + BUILD_NUMBER + "      (" + outDateVersion + ")");
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {

            @Override
            public void run() {
                checkForSystemAlarms();
                if (mySQLConnection != null) {
                    updateNumberOfAlarms();
                    runAlarms();
                } else {
                    openDatabaseConnection();
                }
            }
        }, 0, 60000);
    }

    private static void runAlarms() {
        writeInformationToLogFile("INFO: running " + numberOfAlarms + " alarms.");
        for (int threadNumber = 0; threadNumber < numberOfAlarms; ++threadNumber) {
            createAndRunThread(threadNumber);
        }
    }

    private static void checkForSystemAlarms() {
        checkDiscSpace();
        checkLogFileSize();
    }

    private static void createPDF(int alarmOID, String alarmPDF) throws IOException, InterruptedException {
        WkHtmlToPdf pdf = new WkHtmlToPdf();
        pdf.addSources(Source.fromUrl(alarmPDF));
        pdf.addArguments(Argument.from(Option.GlobalOption.Orientation, "landscape"));
        pdf.addArguments(Argument.from(Option.PageOption.JavascriptDelay, "5000"));
        writeInformationToLogFile("Starting parsing from URL.");
        pdf.save(Paths.get("" + alarmOID + ".pdf"));
        writeInformationToLogFile("Parsed.");
    }

    private static void updateNumberOfAlarms() {
        numberOfAlarms = downloadNumberOfAlarms();
    }

    private static void createAndRunThread(int alarmRowNumber) {
        Runnable task = () -> {
            String query = "SELECT * FROM zapsi2.alarm limit " + alarmRowNumber + ",1";
            String alarmName = "";
            try {
                Statement statement = (Statement) mySQLConnection.createStatement();
                ResultSet actualAlarm = statement.executeQuery(query);
                while (actualAlarm.next()) {
                    int alarmOID = actualAlarm.getInt("OID");
                    alarmName = actualAlarm.getString("Name");
                    String alarmSQLCommand = actualAlarm.getString("Sql");
                    String alarmMessage = actualAlarm.getString("Message");
                    String alarmEmails = actualAlarm.getString("Email");
                    String alarmWebPage = actualAlarm.getString("Url");
                    String alarmPDF = actualAlarm.getString("Pdf");
                    if (alarmMessage == null) {
                        alarmMessage = "Neni nastaven text zpravy";
                    }
                    if (alarmPDF == null) {
                        alarmPDF = "";
                    }
                    if (alarmWebPage == null) {
                        alarmWebPage = "";
                    }
                    writeInformationToLogFile("Running SQL: " + alarmSQLCommand);
                    actualAlarm = statement.executeQuery(alarmSQLCommand);
                    int runningAlarmOID = returnRunningAlarmOID(alarmOID);
                    System.out.println("Running alarm oid is " + runningAlarmOID);

                    if (alarmHasResults(actualAlarm, alarmName)) {
                        System.out.println("\tALARM HAS RESULTS");
                        writeInformationToLogFile(alarmName + ": alarm has results.");
                        if (!alarmIsRunning(runningAlarmOID)) {
                            writeInformationToLogFile(alarmName + ": alarm not running, creating.");
                            System.out.println("\tALARM NOT RUNNING: CREATING");
                            createNewAlarm(alarmOID);
                            if (!alarmPDF.equals("")) {
                                createPDF(alarmOID, alarmPDF);
                            }
                            sendEmail(alarmMessage, alarmEmails, alarmWebPage, alarmPDF, alarmOID);
                            writeInformationToLogFile(alarmName + ": alarm created.");
                        } else {
                            writeInformationToLogFile(alarmName + ": alarm still running.");
                            System.out.println("\tALARM RUNNING");
                        }
                    } else {
                        System.out.println("\tALARM HAS NOT RESULTS");
                        writeInformationToLogFile(alarmName + ": alarm has not results.");
                        if (alarmIsRunning(runningAlarmOID)) {
                            System.out.println("\tALARM IS RUNNING, CLOSING");
                            writeInformationToLogFile(alarmName + ": alarm closing.");
                            closeAlarm(runningAlarmOID);
                        }
                    }
                }
                statement.close();
            } catch (SQLException e) {
                writeInformationToLogFile("ALERT: SQL error in alarm >" + alarmName + "<.");
                sendEmail("Bad SQL command in alarm " + alarmName, emailsTo, "", "", 0);
            } catch (IOException e) {
                writeInformationToLogFile("ALERT: cannot write to or create file.");
                e.printStackTrace();
            } catch (InterruptedException e) {
                writeInformationToLogFile("ALERT: problem creating pdf.");
                e.printStackTrace();
            }
        };
        new Thread(task).start();
    }


    private static void sendEmail(String alarmMessage, String alarmEmails, String alarmWebPage, String alarmPDF, int alarmOID) {
        alarmMessage = customer + ": " + alarmMessage;
        System.out.println("Sending email");
        String[] allEmails = alarmEmails.split(";");
        try {
            MultiPartEmail email = new MultiPartEmail();
            email.setHostName(smtpHost);
            email.setSmtpPort(port);
            email.setAuthenticator(new DefaultAuthenticator(emailsFrom, smtpPassword));
            if (SSL.equals("true")) {
                email.setSSLOnConnect(true);
            } else {
                email.setSSLOnConnect(false);
            }
            email.setFrom(emailsFrom);
            email.setSubject(alarmMessage);

            if (!alarmPDF.equals("")) {
                EmailAttachment attachment = new EmailAttachment();
                attachment.setPath("" + alarmOID + ".pdf");
                attachment.setDisposition(EmailAttachment.ATTACHMENT);
                attachment.setDescription("Zapsi");
                email.attach(attachment);
            }
            email.setMsg(alarmMessage + " " + alarmWebPage);
            email.addTo(allEmails);
            email.send();
            writeInformationToLogFile("Alarm " + alarmMessage + " sent.");
        } catch (EmailException e) {
            System.out.println("Cannot send email");
            writeInformationToLogFile("Cannot send email " + alarmMessage);
        }

    }

    private static void closeAlarm(int runningAlarmOID) throws SQLException {
        String query = "UPDATE `zapsi2`.`alarm_history` SET `DTE`=NOW() WHERE `OID`='" + runningAlarmOID + "';";
        Statement statement = (Statement) mySQLConnection.createStatement();
        statement.executeUpdate(query);
        query = "UPDATE zapsi2.alarm_history set zapsi2.alarm_history.Interval = timediff(now(), DTS) where OID = " + runningAlarmOID;
        statement.executeUpdate(query);
        statement.close();
    }

    private static int returnRunningAlarmOID(int alarmOID) {
        int runningAlarmOID = 0;
        try {
            String query = "SELECT * FROM zapsi2.alarm_history where alarmID=" + alarmOID;
            Statement statement = (Statement) mySQLConnection.createStatement();
            ResultSet actualAlarm = statement.executeQuery(query);
            while (actualAlarm.next()) {
                runningAlarmOID = actualAlarm.getInt("OID");
            }
            statement.close();
        } catch (SQLException e) {
            System.out.println("Cannot download running alarm OID");
        }
        return runningAlarmOID;
    }

    private static void createNewAlarm(int alarmOID) throws SQLException {
        String query = "INSERT INTO `zapsi2`.`alarm_history` (`AlarmID`, `DTS`) VALUES ('" + alarmOID + "', NOW());\n";
        Statement statement = (Statement) mySQLConnection.createStatement();
        statement.executeUpdate(query);
        statement.close();
    }

    private static boolean alarmIsRunning(int alarmOID) throws SQLException {
        boolean alarmIsRunning = false;
        String query = "SELECT * FROM zapsi2.alarm_history where OID = " + alarmOID + " and DTE is NULL ;";
        Statement statement = (Statement) mySQLConnection.createStatement();
        ResultSet numberOfAlarmsInDatabase = statement.executeQuery(query);
        while (numberOfAlarmsInDatabase.next()) {
            alarmIsRunning = true;
        }
        System.out.println("" + alarmOID + " is running: " + alarmIsRunning);
        statement.close();
        return alarmIsRunning;
    }

    private static boolean alarmHasResults(ResultSet actualAlarm, String alarmName) {
        boolean alarmHasResults = false;
        try {
            while (actualAlarm.next()) {
                alarmHasResults = true;
                String OID = actualAlarm.getString("OID");
                String date = actualAlarm.getString("date");
                writeInformationToLogFile(alarmName + ": returned row with OID " + OID + " and date " + date);
                System.out.println("Alarm has results: " + actualAlarm.toString());
            }
        } catch (SQLException e) {
            System.out.println("Cannot download results for alarm");
        }
        return alarmHasResults;
    }

    private static int downloadNumberOfAlarms() {
        int numberOfAlarms = 0;
        String query = "SELECT count(oid) FROM zapsi2.alarm;";
        Statement statement;
        try {
            statement = (Statement) mySQLConnection.createStatement();
            ResultSet numberOfAlarmsInDatabase = statement.executeQuery(query);
            while (numberOfAlarmsInDatabase.next()) {
                numberOfAlarms = numberOfAlarmsInDatabase.getInt("count(oid)");
            }
            statement.close();
            System.out.println("Alarms in database: " + numberOfAlarms);
        } catch (SQLException e) {
            System.out.println("ALERT: database not connected");
        }
        return numberOfAlarms;
    }

    private static void checkLogFileSize() {
        long sizeOfLogFileInBytes = 5000000;
        File file = new File("log\\log.txt");
        File oldFile = new File("log\\log.old");
        if (file.length() > sizeOfLogFileInBytes) {
            if (oldFile.exists()) {
                oldFile.delete();
            }
            System.out.println("File renamed to " + file.renameTo(new File("log\\log.old")));
        }
    }

    private static void createLogFileIfNotExist() {
        try {
            File directory = new File("log");
            if (!directory.exists()) {
                System.out.println("Directory created: " + directory.mkdir());
            }
            File file = new File("log\\log.txt");
            if (!file.exists()) {
                System.out.println("Log file created: " + file.createNewFile());
            }
        } catch (IOException e) {
            System.out.println("Cannot create log folder or log file.");
        }
    }

    private static void checkDiscSpace() {
        String os = System.getProperty("os.name");
        File file;
        if (os.contains("Linux")) {
            file = new File("/");
        } else {
            file = new File("c:");
        }
        long totalSpace = file.getTotalSpace() / 1024 / 1024;
        long freeSpace = file.getFreeSpace() / 1024 / 1024;
        long usagePercentage = freeSpace * 100 / totalSpace;
        System.out.println("Free space is " + usagePercentage + "%");
        if (usagePercentage < 10) {
            sendEmail("Disc free space is under 10%", emailsTo, "", "", 0);
        }
    }

    private static void openDatabaseConnection() {
        try {
            String databaseConnection = "jdbc:mysql://" + databaseAddress + ":3306/" + databaseName + "?autoReconnect=true&useSSL=false";
            mySQLConnection = (Connection) DriverManager.getConnection(databaseConnection, userName, userPassword);
            if (mySQLConnection != null) {
                writeInformationToLogFile("INFO: database connected.");
                System.out.println("INFO: connected");
            }
        } catch (SQLException cannotConnect) {
            System.out.println("Not connected");
            writeInformationToLogFile("ALERT: database not connected.");
            sendEmail("Database not connected", emailsTo, "", "", 0);
        }
    }

    private static void createSettingsFileIfNotExist() {
        File file = new File("config.txt");
        if (!file.exists()) {
            try {
                System.out.println("Config file created: " + file.createNewFile());
                String content = "" +
                        "[databaseAddress] localhost\n" +
                        "[databaseName] zapsi2\n" +
                        "[userName] zapsi_uzivatel\n" +
                        "[userPassword] zapsi\n" +
                        "[deleteInterval] 10\n" +
                        "[customer] zapsi\n" +
                        "[emailsFrom] support@zapsi.eu\n" +
                        "[port] 465\n[emailTo] zapsi@zapsi.eu\n" +
                        "[smtpHost] smtp.forpsi.com\n" +
                        "[smtpPassword] support01..\n" +
                        "[SSL] true";
                FileUtils.writeStringToFile(file, content, UTF_8);
            } catch (IOException e) {
                System.out.println("Cannot write to config file");
            }
        }
    }


    private static void writeInformationToLogFile(String message) {
        try {
            String timeStamp = new SimpleDateFormat("dd.MM.yyyy, HH:mm:ss ").format(Calendar.getInstance().getTime());
            File file = new File("log\\log.txt");
            String content = timeStamp + "- " + message + "\n";
            FileUtils.writeStringToFile(file, content, UTF_8, true);
        } catch (IOException e) {
            System.out.println("Cannot write to log file.");
        }
    }


    private static void loadSettingsFromFile() {
        String actualLine;
        try {
            FileReader fr = new FileReader("config.txt");
            BufferedReader br = new BufferedReader(fr);
            while ((actualLine = br.readLine()) != null) {
                if (actualLine.contains("[databaseAddress]")) {
                    databaseAddress = actualLine.substring(18);
                } else if (actualLine.contains("[databaseName]")) {
                    databaseName = actualLine.substring(15);
                } else if (actualLine.contains("[userName]")) {
                    userName = actualLine.substring(11);
                } else if (actualLine.contains("[userPassword]")) {
                    userPassword = actualLine.substring(15);
                } else if (actualLine.contains("[customer]")) {
                    customer = actualLine.substring(11);
                } else if (actualLine.contains("[emailsFrom]")) {
                    emailsFrom = actualLine.substring(13);
                } else if (actualLine.contains("[port]")) {
                    port = Integer.valueOf(actualLine.substring(7));
                } else if (actualLine.contains("[emailTo]")) {
                    emailsTo = actualLine.substring(10);
                } else if (actualLine.contains("[smtpHost]")) {
                    smtpHost = actualLine.substring(11);
                } else if (actualLine.contains("[smtpPassword]")) {
                    smtpPassword = actualLine.substring(15);
                } else if (actualLine.contains("[SSL]")) {
                    SSL = actualLine.substring(6);
                }
            }
            fr.close();
            br.close();
        } catch (IOException e) {
            System.out.println("Cannot read config file.");
        }
    }
}

