# Zapsi Alarm Server Service
Service that send alarm emails from system made by www.zapsi.eu.
GUI for service is here https://github.com/jahaman/ZapsiAlarmServerGUI

## Features
* check alarms periodically (1 minute interval)
* alarm is an SQL select condition, that is evaluated either to NULL or some result
  * if NULL, no alarm is send, or alarm is closed
  * if NOTNULL, alarm is send, or extended, when already running
* alarm is send to email addresses, set by user
* alarm can create PDF from zapsi web page and send it as an attachement

www.zapsi.eu © 2017

**Examples:**

alarm send by exact time and day: `select * from (SELECT IF(NOW() LIKE '%07:00:%', TRUE,null) and IF(DAYOFWEEK(NOW()) LIKE '%2%', TRUE, null) as result) temptable where result is not null;`

**Information:**

When sending to multiple emails, use ; as a delimiter, for example `first@email.com;second@email.com`


[AlarmServerService.zip](https://github.com/jahaman/ZapsiAlarmServerService/files/1523758/AlarmServerService.zip)
